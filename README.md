Este proyecto es para probar las funcionalidades básicas de [Webpack](webpack.js.org)
siguiendo el [tutorial oficial](https://webpack.js.org/guides/getting-started/).

Loaders usados:
- [css-loader](https://webpack.js.org/loaders/css-loader) para poder import-ar archivos css (y también [css modules](https://webpack.js.org/loaders/css-loader/#modules))
- [style-loader](https://webpack.js.org/loaders/style-loader) para encadenar con el loader anterior y que inserte el css dinámicamente en el HEAD

Para crear el proyecto:
```
npm init -y
npm i -D webpack@5 webpack-cli@4 css-loader style-loader
```

Para correr webpack: `npx webpack`. Esto lee la configuración en `webpack.config.js`.

Mirar los archivos compilados en `dist/bundles`.

Probar la página abriendo `dist/index.html` y mirando la consola.
