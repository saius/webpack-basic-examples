module.exports = {
  entry: {
    'basic': './src/basic.js',
    'import-local': './src/import-local.js',
    'import-external': './src/import-external.js',
    'css-style': './src/css-style.js'
  },
  output: {
    filename: '[name]-bundle.js',
    path: __dirname + '/dist/bundles'
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
};
